def write_to_file(name: str, phrase: str) -> None:
    if type(phrase) is not str:
        raise TypeError("Phrase must be a string type!")

    with open(name, "w") as f:
        f.write(phrase)
