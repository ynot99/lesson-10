from unittest.mock import MagicMock, patch, call

import pytest

from utils.main import write_to_file


@pytest.mark.parametrize(
    "name,phrase",
    [
        ("name1", "phrase1"),
        (f"{'name2'}", f"{'phrase2'}"),
        (b"name3", f"phrase{3}"),
        (4, str(4)),
    ],
)
def test_write_to_file(name, phrase):
    with patch("builtins.open", MagicMock()) as mock_open:
        write_to_file(name, phrase)

        mock_open.assert_has_calls(calls=[call(name, "w")])
        mock_open.return_value.__enter__().write.assert_called_once_with(phrase)


@pytest.mark.parametrize("phrase", [b"OH, HELLO THERE", 1, True])
def test_write_to_file_phrase_type_error(phrase):
    with pytest.raises(TypeError):
        write_to_file("WOAH.txt", phrase)


def test_write_to_file_magic_mock_fixture(magic_mock):
    with magic_mock as mock_open:
        write_to_file("name", "phrase")

        mock_open.assert_called_once_with("name", "w")
        mock_open.return_value.__enter__().write.assert_called_once_with(
            "phrase"
        )
