from unittest.mock import patch, MagicMock

import pytest


@pytest.fixture
def magic_mock() -> MagicMock:
    return patch("builtins.open", MagicMock())
